con = console
fs = require("fs")
path = require("path")
stylus = require("stylus")
jade = require("jade")

ncp = require("ncp").ncp
requirejs = require("requirejs")
# CleanCSS = require("clean-css")
# cordell = require("cordell")


module.exports = (grunt) ->
  grunt.initConfig(
    pkg: grunt.file.readJSON("package.json"),
    stylus:
      compile:
        files:
          "public/css/style.css": "app/stylus/style.styl"
    jade:
      compile:
        files: [
          {
            expand: true
            cwd: "app/jade/"
            src: ["**/*.jade", "!**/_**"]
            dest: "public/"
            ext: ".html"
          }
          {
            expand: true
            cwd: "app/templates/"
            src: ["**/*.jade", "!**/_**"]
            dest: "public/templates"
            ext: ".html"
          }
        ]
        options:
          cwd: "app/"
    coffee:
      compile:
        files: [
          {
            expand: true
            cwd: "app/coffee/"
            src: ["**/*.coffee"]
            dest: "public/js"
            ext: ".js"
          }
        ]

    copy:
      main:
        files: [
          {
            expand: true
            cwd: "app/assets"
            src: ["**"]
            dest: "public/"
          }
        ]

    watch:
      options:
          livereload: true
      assets:
        files: ["app/assets/**/*.*"]
        tasks: ["newer:copy"]
        options:
          spawn: false
      css:
        files: ["app/stylus/**/*.styl"]
        tasks: ["stylus"]
        options:
          spawn: false
      coffee:
        files: ["app/coffee/**/*.coffee"] 
        tasks: ["newer:coffee"]
        options:
          spawn: false
      jade:
        files: ["app/jade/**/*.jade", "app/templates/**/*.jade", "!app/jade/**/_*.jade"] 
        tasks: ["newer:jade"]
        options:
          spawn: false
      mixins:
        files: ["app/jade/**/_*.jade"] 
        tasks: ["jade"]
        options:
          spawn: false
      
    connect:
      server:
        options:
          port: 9001,
          base: "public"
  )

  grunt.loadNpmTasks("grunt-contrib-stylus");
  grunt.loadNpmTasks("grunt-contrib-jade");
  grunt.loadNpmTasks("grunt-contrib-coffee");
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-newer");
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask("build", ["copy", "stylus", "jade", "coffee"])
  grunt.registerTask("default", ["connect", "watch"])

  # -----------------------------------
  # 
  # HELPERS
  # 

  parseCLArgs = () ->
    o = {}
    for f in grunt.option.flags()
      try 
        pieces = f.split("=")
        val = pieces[1]
        pieces = pieces[0].split("-")
        key = pieces[pieces.length - 1]
        o[key] = val
      catch error
        con.log("Error parsing command line argument", f)
    return o

  writeFile = (filePath, contents, callback) ->
    try
      fs.writeFileSync(filePath, contents, {encoding:"utf8", mode:"0777"})
    catch e
      con.log(e)
    callback() if callback?

  makePath = (dirPath, mode, callback) ->
    try
      fs.mkdirSync(dirPath, mode)
    catch e
      con.log(e) unless e.errno is 47
    callback() if callback?

  saveSpriteImage = (dirPath, dir, v, imagePath, k) ->
    makePath(dirPath, "0777", (error) ->
      copyFileSync(dir + "/" + v.filename(), imagePath)
    )

  copyFileSync = (srcFile, destFile) ->
    BUF_LENGTH = 64*1024
    buff = new Buffer(BUF_LENGTH)
    fdr = fs.openSync(srcFile, 'r')
    fdw = fs.openSync(destFile, 'w')
    bytesRead = 1
    pos = 0
    while bytesRead > 0
      bytesRead = fs.readSync(fdr, buff, 0, BUF_LENGTH, pos)
      fs.writeSync(fdw,buff,0,bytesRead)
      pos += bytesRead
    fs.closeSync(fdr)
    fs.closeSync(fdw)

  copyFile = (src, dest) ->
    fs.createReadStream(src).pipe(fs.createWriteStream(dest))