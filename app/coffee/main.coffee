con = console

init = () ->
	stickyNavTop = $('.nav-bar').offset().top
	
	stickyNav = () ->
		navBar = $('.nav-bar')
		mainBody = $('.main-body')
		scrollTop = $(window).scrollTop()

		if scrollTop > stickyNavTop
			navBar.addClass("fixed-nav-bar")
			mainBody.addClass("adjust-nav-bar")
		else
			navBar.removeClass("fixed-nav-bar")
			mainBody.removeClass("adjust-nav-bar")

	stickyNav()

	$(window).scroll () ->
		stickyNav()

$(document).ready(init)

